'use strict';

var BookController = function($scope, $http) {
    $scope.book = {};
    $scope.editMode = false;

    $scope.fetchBooksList = function() {
        $http.get('books/bookslist.json').success(function(bookList){
            $scope.books = bookList;
        });
    };

    $scope.addNewBook = function(book) {
        $scope.resetError();

        $http.post('books/addBook', book).success(function() {
            $scope.fetchBooksList();
            $scope.book.bookname = '';
            $scope.book.authorName = '';
        }).error(function() {
            $scope.setError('Could not add a new book');
        });
    };


    $scope.removeBook = function(book) {
        $scope.resetError();

        $http.post('books/removeBook/', book).success(function() {
            $scope.fetchBooksList();
            $scope.book.bookname = '';
            $scope.book.authorName = '';
        }).error(function() {
            $scope.setError('Could not remove book');
        });
    };

    $scope.resetBookForm = function() {
        $scope.resetError();
        $scope.book = {};
        $scope.editMode = false;
    };

    $scope.resetError = function() {
        $scope.error = false;
        $scope.errorMessage = '';
    };

    $scope.setError = function(message) {
        $scope.error = true;
        $scope.errorMessage = message;
    };

    $scope.fetchBooksList();

};