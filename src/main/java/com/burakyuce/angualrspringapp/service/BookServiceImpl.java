package com.burakyuce.angualrspringapp.service;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import org.springframework.stereotype.Service;

import com.burakyuce.angualrspringapp.beans.BookEntity;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.core.util.MultivaluedMapImpl;

@Service("bookService")
public class BookServiceImpl implements BookService {

    @Override
    public List<BookEntity> getAllBooks() {

        List<BookEntity> bookList = new ArrayList<BookEntity>();
        try {

            ClientConfig clientConfig = new DefaultClientConfig();
            clientConfig.getClasses().add(JacksonJsonProvider.class);
            Client client = Client.create(clientConfig);

            List<BookEntity> list = client.resource("http://dataservices-analyser.rhcloud.com/book/findAll").get(new GenericType<List<BookEntity>>() {
            });

            bookList.addAll(list);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return bookList;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Override
    public void addBook(BookEntity entity) {
        try {

            Client client = Client.create();
            WebResource webResource = client.resource("http://dataservices-analyser.rhcloud.com/book/saveOrUpdate");

            MultivaluedMap formData = new MultivaluedMapImpl();
            formData.add("bookName", entity.getBookName());
            formData.add("authorName", entity.getAuthorName());

            webResource.type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).post(ClientResponse.class, formData);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Override
    public void deleteBook(BookEntity entity) {
        try {

            Client client = Client.create();
            WebResource webResource = client.resource("http://dataservices-analyser.rhcloud.com/book/remove");

            MultivaluedMap formData = new MultivaluedMapImpl();
            formData.add("bookName", entity.getBookName());

            webResource.type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).post(ClientResponse.class, formData);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
