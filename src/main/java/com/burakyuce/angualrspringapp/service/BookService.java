package com.burakyuce.angualrspringapp.service;

import java.util.List;

import com.burakyuce.angualrspringapp.beans.BookEntity;

public interface BookService {

    public List<BookEntity> getAllBooks();

    public void addBook(BookEntity entity);

    public void deleteBook(BookEntity entity);

}
