package com.burakyuce.angualrspringapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.burakyuce.angualrspringapp.beans.BookEntity;
import com.burakyuce.angualrspringapp.service.BookService;

@Controller
@RequestMapping("/books")
public class BookController {

    @Autowired
    private BookService bookService;

    @RequestMapping("bookslist.json")
    public @ResponseBody List<BookEntity> getBookList() {
        return bookService.getAllBooks();
    }

    @RequestMapping(value = "/addBook", method = RequestMethod.POST)
    public @ResponseBody void addBook(@RequestBody BookEntity book) {
        bookService.addBook(book);
    }

    @RequestMapping(value = "/removeBook", method = RequestMethod.POST)
    public @ResponseBody void removeBook(@RequestBody BookEntity book) {
        bookService.deleteBook(book);
    }

    @RequestMapping("/layout")
    public String getBookPartialPage(ModelMap modelMap) {
        return "books/layout";
    }
}
